Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mes
Source: https://git.savannah.gnu.org/cgit/mes.git

Files:     *
Copyright: 1989-2016 Free Software Foundation, Inc.
           1993-2010 Dominique Boucher
           2014-2024 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
           2018 Han-Wen Nienhuys <hanwen@xs4all.nl>
           2016-2017 Jeremiah Orians
           2016-2019 Jeremiah Orians <jeremiah@pdp10.guru>
           2008 Derek Peschel
           2015 David Thompson <davet@gnu.org>
           2005-2007 Oleg Kiselyov
           2007 Daniel P. Friedman
           2004 Andy Wingo <wingo at pobox dot com>
           2001 Oleg Kiselyov <oleg at pobox dot com>
           1991, Marc Feeley
           2019-2021 Danny Milosavljevic <dannym@scratchpost.org>
           2020 Vagrant Cascadian <vagrant@reproducible-builds.org>
           2021-2024 Andrius Štikonas
           2021 W. J. van der Laan <laanwj@protonmail.com>
           2021 Wladimir van der Laan <laanwj@protonmail.com>
           2022 Dor Askayo <dor.askayo@gmail.com>
           2022-2023 Rick Masters <grick23@gmail.com>
           2022-2024 Ekaitz Zarraga <ekaitz@elenq.tech>
           2022 Gabriel Wicki <gabriel@erlikon.ch>
           2022-2023 Timothy Sample <samplet@ngyro.com>
           2023 Andrius Štikonas <andrius@stikonas.eu>
           2023 Emily Trau <emily@downunderctf.com>
           2023 Vagrant Cascadian <vagrant@debian.org>
           2024 Michael Forney <mforney@mforney.org>
License:   GPL-3.0+

Files:     mes/module/mes/quasisyntax.scm
Copyright: 2006 Andre van Tonder.
License:   Expat

Files:     lib/posix/getopt.c
Copyright: 1987-1992 Free Software Foundation, Inc.
           2017-2018 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
License:   GPL-2.0+

Files:     mes/module/mes/psyntax.ss
Copyright: 1992-1997 Cadence Research Systems
           2001-2006 Free Software Foundation, Inc.
License: GPL-3.0+ and PERMISSIVE-alt

Files:     BOOTSTRAP
           HACKING
           INSTALL
           NEWS
           PORTING
           ROADMAP
           include/linux/SYSCALLS
Copyright: 2016-2024 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
License:   PERMISSIVE

Files:     mes/module/mes/syntax.scm
Copyright: 1993-2004 Richard Kelsey and Jonathan Rees
           2016 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
License:   GPL-3.0+ and BSD-3-Clause

Files:     doc/mes.texi
Copyright: 2018-2023 Jan (janneke) Nieuwenhuizen
License: GFDL-1.3+

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 3. The name of the authors may not be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License:   Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License:   GPL-2.0+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License:   GPL-3.0+
 Mes is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 Mes is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with Mes.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License:   PERMISSIVE
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.

License: PERMISSIVE-alt
 Permission to copy this software, in whole or in part, to use this
 software for any lawful purpose, and to redistribute this software
 is granted subject to the restriction that all copies made of this
 software must include this copyright notice in full.

License: GFDL-1.3+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
 copy of the license is included in the section entitled GNU Free
 Documentation License.
